\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{ME3E course, force method, exercise, homework 6}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt

The examples complement section 3 of the lecture notes
'force method' on the course web page. They
should substitute classes of the present 6th week of the semester.
Questions concerning the stuff, address please to
'petr.rericha@fsv.cvut.cz'. HW 6 should be delivered at the same address
by the next tuesday, March, 30th. If possible, process the homework
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. 

The contribution of the normal and shear forces to deflections is
neglected in the examples unless otherwise stated. The same applies
to the homeworks. Internal forces, deformations, deflections
 and other quantities in the primary structure owing to given load
are indicated by subscript $_0$. Virtual external and internal forces 
are not marked by prefix $\delta$ but indicated by subscript $_1$.
They are always the same as the actual  external and internal forces
in the primary structure loaded by the unit redundant force.
The load case $_1$ and its
quantities with subscripts $_1$ play thus two roles in the force method. 

The cross-section bending stiffness $EJ=1$ is assumed in most examples
for brevity and the contribution of the normal and shear forces to
deflections is neglected. 

\section{Frames with a single redundant}
{\bf Frame 1}\\
LC1, force load:\\
\insFigUp{frame1lc1.pdf}{\textwidth}
\\
Deflections $\Delta_0$ in the primary structure owing to given load and
$\delta$ owing to unit redundant force are:
\[ \Delta_0= \int M_0M_1\dd x={1\over 3}2\cdot24\cdot1\cdot5=83.3,~~~
 \delta=\int M_1^2\dd x={1\over 3}2\cdot1^2\cdot5=8.33 \]
and the compatibility equation $\Delta_0+X\delta-0$ yields
$X=-10$.
Note that $\Delta_0$ and $ \delta$ both are to be divided by
the cross-section bending stiffness $EJ$ to obtain true deflections
but the redundant force $X$ does not depend on $EJ$!
\[ M= M_0+XM_1\]
\emph{The next load cases are all without force loads. It implies
that the resulting bending moment diagrams are affine to $M_1$
(see the above equation). They are thus not presented for brevity.}
LC2, homogeneous temperature change:\\
The normal forces in the load case $_1$ are easy to derive in the $M_1$
diagram above. The only nonzero, $N_1=0.2$ normal force is in the
central horizontal part with the hinge. Thus
\[  \Delta_0= \int \alpha t N_1 \dd x= \alpha t \cdot 0.2\cdot5=
\alpha t,~~~~X=-{\alpha t \over 8.33}=-0.12\alpha t~~(\cdot EJ) \]
LC3, temperature gradient $\Delta t$ in the horizontal beam:\\
The beam depth is $h$. 
\[ \Delta_0=\int \kappa_t M_1\dd x= -{\Delta t \alpha \over h}\cdot 1\cdot5
,~~~\Rightarrow ~~ X=0.6{\Delta t \alpha \over h} \]
LC4, vertical settlement $s$ of the right pin support\\
The displacement $\Delta_0$ in the primary structure conjugate
to $X$ (relative rotation of the left and right
parts of the primary structure in this case) is not known but can be
determined in terms of $s$ using the PVW.
Virtual work of forces in load case $M_1$ upon actual displacements
in the left sketch below is
\[ \delta w=\Delta_0 \cdot 1 + 0\cdot s=0,~~~\Rightarrow~~\Delta_0=0.\]
The compatibility equation then yields $X=0$
and we conclude that this load does not induce any forces in the frame.
\\
\insFigUp{frame1lc4.pdf}{\textwidth}
\\
LC5, horizontal slide $s$ of the right pin support\\
This time the expression for the virtual work of the forces in
$M_1$ load case upon the displacements in the right sketch above reads
\[  \delta w=\Delta_0 1 + 0.20\cdot s=0,~~~\Rightarrow~~\Delta_0=-0.2s. \]
The compatibility equation yields $X=0.2s/\delta=0.2s/8.33=0.024s$.
\\ \\
Note that the $\delta$ deflection remains the same for all load cases,
it is associated with the primary structure.
\\ \\
It is recommended that students solve some of the above load cases using
another primary structure.
\\ \\
{\bf Frame 2}
\\
\insFigUp{frame2ps.pdf}{\textwidth}
\\
Using the default assumptions on the effect of the normal forces,
$\Delta_0=0$ since $M_0=0$. Further,
\[ \delta= \int M_1^2 \dd x= 1^2(4+5)/3=3(\cdot 1/(EJ)) \]
and thus $X=0$. \emph{There are no internal forces in this frame if the
contributions of the normal and shear forces to deflections are neglected.}
The contribution of the normal force is accounted for next.
It turns out that it is not trivial to compute $N_1$ forces in the
primary structure. The problem may occur in drawing $N$ diagrams in
frames generally so the solution is shown in this example.
First, shear forces have to be determined in load case $_1$, see
$M_1$ diagram above, from beam equilibrium equations:
\[ beam 2,3, \odot 2: -1 -V_{3,2}\cdot4=0,~~\Rightarrow~~V_{3,2}=-0.25, \]
\[ beam 1,3, \odot 3: 1-V_{3,1}\cdot 5=0,~~\Rightarrow~~V_{3,1}=0.20  \]
Next, normal forces by joint 3 equilibrium:
\[ 3\uparrow: -V_{3,2}-V_{3,1}\cdot 4/5 +N_{3,1}\cdot 3/5=0,~~~
 \Rightarrow~~N_{3,1}=-0.15, \]
\[ 3\rightarrow: N_{3,1}\cdot 4/5 +V_{3,1}\cdot 3/5 +N_{3,2}=0,~~~
\Rightarrow~~N_{3,2}=0, \]
The $N_1$ diagram is the rightmost in the above sketch.
The relative rotation at joint 3 in the primary structure then is:
\[ \Delta_0=\int \bar \varepsilon_0 N_1 \dd x=\int N_0N_1/(EA)\dd x, \]
so that $N_{3,1}$ is necessary in LC $_0$, see the second sketch above.
It is $N_{3,1}= -1\cdot 5/3=$ by equil. of joint 3 in vertical 
direction and $\Delta_0= -5/3\cdot(-0.15)\cdot5=1.25(\cdot 1/(EA))$.
The physical meaning of $\Delta_0$ is the relative rotation of the
two beams of the primary structure loaded by the given load (unit force
at joint 3) \emph{when the effect of the normal forces is accounted for}.
The redundant, the bending moment at joint 3, finally is
\[ X= {-1.25\cdot 1/(EA) \over \delta}= -0.416{J\over A} \]
The moment vanishes when $J\rightarrow 0$, then the frame becomes a truss.
The final bending moment diagram is by superposition
\[ M= X M_1= -0.416{J\over A}M_1,\]
so that the shape is that of $M_1$ with ordinates by the above expression.

\section{HW 6}
Solve the frames $a$ and $b$ by the force method, each for two actual
load cases. Load case 2 in frame $a$ is a homogeneous temeperature
change $t=20$ in the whole frame with $\alpha=0.1$, $EJ=1$, in frame $b$
it is the support settlement of the roller $s=0.1$.
\\
\insFigUp{hw6-1redundant.pdf}{\textwidth}
\newpage
HW6 - solution:
\\ {\bf task a:}
\\
\insFigUp{hw6-1redundant-sol-a.pdf}{\textwidth}
\\
LC 1:
\[ \Delta_0= 12\cdot720\cdot0.5(1/3+1/2+1/6)=4320 (\cdot{1\over EJ}) \]
\[ \delta= {1 \over 3} 1 (24+18)= 14 (\cdot{1\over EJ}),~~~\Rightarrow~~
  X=-{4320\over 14}= -308 \]
\\
LC 2: \\
\[\Delta_0= 20\alpha(0.0555\cdot24+0.042\cdot 18)=41.6\alpha \]
\[X= -{41.6\alpha \over 14} EJ = -2.97\alpha EJ \]
The final bending moment diagram is affine to $M_1$ with factor $X$.
\\ \\
{\bf task b:}
\\
\insFigUp{hw6-1redundant-sol-b.pdf}{\textwidth}
\\
LC 1:
\\
Integral $\int M_0M_1\dd x$ is difficult to evaluate by decomposition
in elementary shapes of the $M_0$ and $M_1$ functions. Exceptionally,
the direct integration is better. Three intervals are considered:
\\ I: $M_0=3.5x-x^2/2$, $M_1= 2x/7$, $\int M_0M_1\dd x=\int(-x^2+x^3/7)=
 -19.3$
\\ II: $M_0=3.5.x-x^2/2$, $M_1=-5x/7$, $\int (-2.5x^2+5x^3/14)\dd x=
 -5.24$
\\ III: $-{1\over 3}{5\over 7}\cdot 2\cdot 7(2+5)=-23.3$
\\ $\Delta_0= -47.8\left({1\over EJ}\right)$, 
 $\delta= 3{1\over 3}\left({10\over 7}\right)^2\cdot 7
  =9.5\left({1\over EJ}\right)$
\[X= {47.8\over 9.5}= 5 \] 
The final $M$ diagram at the bottom is the superposition $M_0+XM_1$.

It is recommended to solve the task with another primary structure.
When the relative rotation is released above support $B$
(a hinge inserted there), then both $M_0$ and $M_1$ diagrams consist
of simple shapes and the table of prefabricate integrals can be used.
\\
LC 2:
\\
The displaced shape of the primary structure when the settlement $s$ is
imposed is shown in the second sketch in the right column of sketches.
The displacement conjugate to the redundant $X$ is $\Delta= -0.1$.
Compatibility condition requires that the deflection of the primary
structure in the direction of the redundant $X$ restores $\Delta$
so that displacement at the support is zero.
\[ X\delta +\Delta =0,~~~\Rightarrow~~ X=0.0105EJ\]
The final bending moment diagram in LC 2 is $M_1\cdot0.0105EJ$.
 
 



\end{document}
