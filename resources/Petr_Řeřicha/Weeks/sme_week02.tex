\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{SM3E course, deflections in planar frames, week 2 exercise}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The topics of the second week exercise are
\begin{enumerate}
\item Deflection of beam by integration of the differential equations
\item Deflection of beam by conjugate beam
\item Application of the principle of virtual work
\end{enumerate}
The homework of this week should arrive at my mailbox on Mar 2nd at
the latest, see the 'Rules of the SM3E course'.
\\
You are welcome to ask questions concerning the stuff any time at my
mailbox 
\\'petr.rericha@fsv.cvut.cz'.\\
The examples complement sections 3 and 4 of the lecture notes
'deflections in plane frames' on the course web page. Together they
should substitute classes of the present 2nd week of the semester.
\\
\section{Deflection by integration of differential equations}
The equations to be integrated are repeated here for easy reference.
Geometric: 
\[ \kappa(x)= - {\dd^2v(x) \over \dd x^2}=-v''(x) \]
Geometric + constitutive:
\[ M(x)= EJ\kappa(x)= -EJ {\dd^2v(x)\over\dd x} =-EJ v''(x)\]
Short form of the derivative is preferred forthwith.
Recall that the contribution of $V$ and $N$ to the deflection is neglected
in section 3 of the Notes and so it is here. 
Only horizontal straight beams are considered in this section,
it is thus suitable to use some ad hoc sign conventions,
consistent with the Notes:
a) bending moment $M$ and curvature $\kappa$ are positive 
when inducing tension in lower face,
b) deflection $v(x)$ is positive downwards (opposite to cartesian
 $y$ axis).
\\
Example 1:\\
Deflection of the simple beam is computed assuming $EJ=1$ for
convenience. The bending moment is computed and drawn first.
\\
\insFigUp{week2-integration1.pdf}{\textwidth}
\\
Function $M(x)$ is needed for the integration. It is different in two
stretches $A-B$ and $B-C$ of the beam. In each of them the
local position variable $x$ is used, do not confuse the two variables! 
\\
Stretch $A-B$ is treated first:
\[ M(x)=-1/8x=-{\dd^2 v(x)\over \dd x^2 }=-v''_{AB} \]
\[ v'_{AB}= 1/16x^2 + c_1,~~~v_{AB}=1/48x^3+ c_1x+c_2 \]
Boundary conditions $v_{AB}(0)=0,~~v_{AB}(4)=0$ determine
$c_1=-1/3,~~c_2=0$. The deflection and the slope of the deflection
in stretch $A-B$ thus are
\[ v_{AB}=1/48(x^3-16x),~~~v'_{AB}=1/48(3x^2-16),~~~v_{AB}(2)=-0.5 \]
Stretch $B-C$:\\
\[ M(x)=-1/2(1-x)^2=-v''_{BC},~~~v'_{BC}=-1/6(1-x)^3+c_1 \]
At $B$ the slopes of $v_{AB}$ and $v_{BC}$ must be the same
to keep the deflection smooth at the point.
\[ 1/48(3\cdot 4^2-16)=-1/6+c_1,~~\Rightarrow c_1=5/6 \]
\[ v_{BC}= 1/24(1-x)^4 +5/6x + c_2 \]
The other boundary condition $v_{BC}(0)=0$ yields $c_2=-1/24$.
\[v_{BC}= 1/24(1-x)^4+5/6xr-/24, v_{BC}(1)=0.79 \]

\section{Deflection by the conjugate beam}
Example 2:\\
Deflection is computed by the conjugate beam method in the continuous
beam, cross-section bending stiffness $EJ=1$ for convenience:
\\
\insFigUp{week2-conjug.pdf}{0.8\textwidth}
\\
The bending moment diagram in the original beam is drawn first.
It is simple in this case. Then the conjugate beam is developed
using the analogies in Table 1 in the Notes. The load of the conjugate
beam is the bending moment in the original beam divided by $EJ$,
in this case it is bending moment diagram itself. Note that in the
ad hoc sign convention the $M$ in the original beam is negative
so the load on the conjugate beam is negative, acting \emph{upwards},
although it is drawn on the upper side of the beam just to avoid
interference with the reactions in hinges $D$ and $E$.
The reactions are shown in order
to facilitate the evalution of the bending moments in the conjugate
beam. Discrete ordinates of it are now easy to evaluate. Three
computed values together with three zero values at the hinges and at
support $B$
are enough to draw the deflection line. The deflection
line of the original beam is  straight in $BC$ and $CD$ intervals,
cubic parabola in intervals $DE$ and $EF$.
The value at the center of the stretch $DE$ is computed:
\[ M_c=-1.33\cdot 2 + 2\dot 1 /2\cdot 1/3\cdot4= -2 \]
\\
\section{Principle of virtual work}
Experience suggests that students do not understand well the 
principle of virtual work (PVW forthwith) and cannot apply it.
The principle is the basis of the current methods of structural
mechanics and structural analysis computer codes.
It is rather abstract but some applications turn out to be simple
and useful in practice.

To begin with, the work is specified through several examples:
\\
\insFigUp{week2-work1.pdf}{0.8\textwidth}
\\
The work of a force on a displacement of a point in sketch $a$ is 
$w=\vec{F}\bold{\cdot}\vec{u}$, if the vectors are parallel then
$w=Fu$ where $F$ and $u$ are the vector's magnitudes.
In plane problems the work of a moment on a body rotation
is $w=M\varphi$ in sketch $b$. 
The work of a lateral load $f$ on a beam deflection $v$ is
$\int f(x)v(x)\dd x$, see sketch $c$. 
\\These are all examples of the work of external
forces done on the body displacements. This work is supplied by some 
external agency when a body is subject to deformation. The work is
stored in the form of the work of stresses or internal forces
inside the body. 
\\An example how the work of the bending moment
can be evaluated is in sketch $d$. When a differential length of
a beam, loaded by two end moments in equilibrium, is subject
to end sections rotations $\gamma$ and $\gamma +\dd \gamma$,
the work of the external forces is
$\dd w=-M\gamma+M(\gamma +\dd \gamma)= M\dd \gamma) $
Geometric equation holds $\dd \gamma= (\dd \gamma / \dd x) \dd x=
\kappa \dd x$ where $\kappa$ is the curvature due to bending.
The work done by the bending moment in a differential length
of a beam thus is $\dd w= M\kappa \dd x$ and in a finite
length of the beam $w=\int M\kappa \dd x$. In a similar way
the work of shear and normal force can be derived
$w_V=\int V \psi \dd x,~~~w_N=\int N \varepsilon \dd x$.
$\psi$ and $\varepsilon$ are the shear deformation and axial
beam axis deformation of the beam respectively. 

In the examples to
follow, some virtual cross-section deformations $\kappa$, $\psi$
or $\varepsilon$ are singular - at a cross-section
they cumulate to an infinite value on a infinitesimal distance
creating a discontinuity. When it happens with curvature then
the cumulated curvature makes a break in the deflection line,
when it happens with the shear deformation a jump of the deflection
line occurs. It is a subtle issue in mathematics, but the work done
is simply  the product of the internal force at the cross-section
and the respective discontinuity, for instance bending moment times
the relative rotation of the left and right sides of the break or
shear force times the jump in the deflection.  
\\
\subsection{Examples of virtual work to check the PVW}
The principle of virtual work, Principle 1 in sec.~4 of the Notes,
is so important that it si worth it to check it in a  few examples.
The versatility of the principle will also be demonstrated.
\\
Example 3\\
Consider two load cases of a cantilever as specified in the left
two sketches of the figure. 
\\
\insFigUp{week2-virt1.pdf}{0.8\textwidth}
\\
External and internal forces are in 
equilibrium so that both load cases are eligible virtual stress
states (recall that equilibrium is the only quallity required of
a virtual stress state). In the right sketch there are two rather
arbitrary virtual displacements. Virtual they are indeed since
such displacements cannot occur in real structures. 
The work of the forces of LC1 and LC2 on  $\delta v_1$ and
$\delta v_2$ is (ad hoc assumption is adopted on the sign of the
bending moments, positive when tension at lower face) :
\\
LC1 on $\delta v_1$: $ l\cdot \delta \alpha - 1 \delta \alpha l=0 $~~~
LC1 on $\delta v_2$: $ -V_c \cdot \delta v +1\cdot \delta v =0 $
\\
LC2 on $\delta v_1$: $ -1\cdot \delta \alpha +1\cdot \delta \alpha =0$~~~
LC2 on $\delta v_2$: $  1\cdot 0 + 0 \cdot \delta v =0 $
\\
The first expressions in the left sides are the work of internal forces,
the second ones are the work of external forces (loads in this case).
Note that the work of internal forces is subtracted since it is the
work stored in he structure. Recall also the explanation of the work
on discontinuous virtual displacements above.
The last example shows a less 'exotic' virtual displacement.
\\
\insFigUp{week2-virt2.pdf}{0.6\textwidth}
\\
An actual load state is shown in the left sketch, a virtual ddeflection
with constant curvature $\delta \kappa=1/R$ is considered. 
The inclination of the deflection line  at supports is 
$\delta \alpha= \pm \delta \kappa l/2$ (it is 0 at the center and
grows with constant rate (constant curvature) towards the supports. The virtual work of the
actual forces  on virtual displacements and deformtions is
\[ \delta w= -1\cdot \delta \alpha - \int M \delta \kappa \dd x=
-1\cdot \delta \kappa l/2 + 1/2\cdot 1\cdot l \cdot \delta \kappa=0 \]
The first term in the expressions is the work of the couple $M=1$,
the work of external forces, the other term is the work of the bending
moment $M$.
\\
Homework 2:
\\ \\
\begin{minipage}{0.45\textwidth}
a)
Derive and draw the deflection line by integration, assume $EJ=1$:\\
\\
\insFigUp{week2-hw-int.pdf}{0.9\textwidth}
\end{minipage}
~~~~
\begin{minipage}{0.45\textwidth}
b)
Derive and draw by the conjugate beam method the deflection line 
of Example~1 in section~1 of the above exercises, solved there by
integration.
\end{minipage}
\\
c) Evaluate the virtual work of the actual stress state in the left 
sketch on the three virtual displacements: 
\\
\insFigUp{week2-hw-virt.pdf}{\textwidth}

\newpage
Homework 2 solutions:
\\
a)\\
\insFigUp{week2-hw-int-sol.pdf}{0.5\textwidth}
\\
Three functions have to be derived in intervals I, II and III.
\\ interval I:\\
Boundary conditions $v(0)=0,~~~v'(0)=0$
\[ M= 0.5-0.25x,~~~v'_I=-(0.5x-0.125x^2 +c_1),~~~c_1=0\]
\[ v_I=-(0.25x^2-0.0416x^3+c_2),~~~c_2=0, v_I=-(0.25x^2-0.0416x^3),~~~
   v_I(2)=-0.66 \]
\\ interval II:\\
Boundary conditions $v_{II}(0)=v_I(2),~~v_{II}(2)=0$
\[ M=-0.25x,~~~v'_{II}=-(-x^2/8+c_1),v_{II}=x^3/24-c_1x+c_2,~~c_2=-0.66,~~
c_1=-1/6\]
\[v_{II}=x^3/24+x/6-2/3,~~v'_{II}=x^2/8+1/6,~~v'_{II}(2)=2/3\]
\\ interval III, note the opposite direction of the local $x$:\\
Boundary conditions $v_{III}(1)=0,~~~v'_{III}(1)=-v'_{II}(2)=-2/3$
\[ M=-x^2/2,~~v'_{III}=x^3/6+c_1,~~c_1=-5/6,~~v'_{III}=x^3/6-5/6 \]
\[  v_{III}=x^4/24-5x/6+c_2,~~ c_2=19/24,~~v_{III}=x^4/24 -5x/6+19/24,~~
   v_{III}(0)=19/24=0.79 \]
\\
b)
\\
\insFigUp{week2-hw-conj-sol.pdf}{0.8\textwidth}
\\
Bending moment on the actual beam is the one in Example 1.
Deflection at the center of stretch $AB$ equals the bending moment 
in the conjugate beam 
$v_c=-(0.33\cdot 2 - 1/3\cdot 2\cdot 0.25 \cdot 2/2)=-0.5$. At the tip
of the cantilever it is
$v_C=-(-0.66\cdot 1 - \int_0^1 0.5x^2\cdot x \dd x=  0.66+0.125=0.79$.
Compare to the result in Example 1.
\\
c)
\\
\insFigUp{week2-hw-virt-sol.pdf}{0.4\textwidth}
\\
$ \delta v_1: -M/l\cdot 1 +M\cdot1/l =0 $, work of the right reaction and
of the loading couple, both external forces.
\\
$\delta v_2:$ 
There are two options, the couple $M$ acting on the left (inclined) 
half or on the right half. In the first case
$\delta v_2: -M\cdot 1/(l/2)-(-V\cdot 1-M/2\cdot 2/l)=
 -2M/l+M/l+M/l=0 $ The first term is the work of the loading couple,
 the second is the work of the shear force on the deflection
 jump and the third is the work of the bending moment (negative past
 the jump section) on the relative rotation of the two parts
 (positive).
\\
$\delta v_3: -M/l\cdot 1 -M/l\cdot 1 -(-V\cdot 2)=
-M/l-M/l+2M/l=0$, The two first terms are work of the reactions,
the last term is the work of the shear force.





\end{document}
