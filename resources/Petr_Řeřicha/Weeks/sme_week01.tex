\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}
%\newlength{\figwidth}
%\newcommand{\insinFig}[3]
%   {\makebox[0pt][l]{\raisebox{-#1\figwidth}{\hspace{#2\figwidth} #3}}}
%\newcommand{\insFig}[2]
%   {\makebox[#2][l]{\resizebox{#2}{!}
%   {\rotatebox{-90}{\includegraphics{#1}}}}}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{SM3E course, deflections in planar frames, week 1 exercise}
\author{Petr \v Re\v richa}
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt
The first week exercise introduces common notation, conventions,
and graphic symbols to be used in the course and reviews the
preliminary knowledge on the solution of statically determinate
planar frames and trusses for reactions in supports and for the
internal forces and their diagrams. Students registered for this course
are assumed to have mastered these topics to the level of active
application in examples. The review should refresh the skills and
unify the approaches.

This week exercise does not refer to the Notes on the faculty web.
\\
The homework of this week should arrive at my mailbox on Feb 23rd at
the latest, see the 'Rules of the SM3E course'.
\\
You are welcome to ask questions concerning the stuff any time at my
mailbox 
\\'petr.rericha@fsv.cvut.cz'.\\
%The examples complement sections 5, 6 and 7 of the lecture notes
%when 
%'deflections in plane frames' on the course web page. They
%should substitute classes of the present 4th week of the semester.
\\
If possible, process the homeworks
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. If you scan or photograph the homework use
\begin{mylist}{-2pt}{10pt}
\item .pdf or .jpg format
\item an adequate resolution so that the file is not larger than 2~MB.  
\item white paper, do not use grid and other background prints
\end{mylist}
Distant teaching can never compare 
to live classes but we will do the best in the circumstances.
\\
\section{Notation, conventions, statical determinacy}
Beams and trusses are specified by their axes in figures but for
exceptions when the cross-section(s) need to be shown. 
\subsection{Frames}
Frequent types of supports and constraints are displayed in the figure,
the number of the degrees of freedom (DOFs) they constrain in parentheses:
\\
\insFigUp{week1-frames.pdf}{\textwidth}
\\
Other types of constraints will be added on occasions.
A short resume on statical determinacy is useful.
\begin{enumerate}
\item A rigid body of whatever shape has three degrees of freedom (DOFs)
in plane, that is, its position can be determined by three independent
parameters, very often two coordinates of a definite point of the body
and a rotation with respect to that point. A point in plane has two DOFs
in plane, its position is determined by two coordinates.
\item Three independent equilibrium conditions can be written down for each
body in plane, at least one of them is a moment equilibrium condition.
Two independent conditions always exist for a point in plane. 
Note the word independent.
For any body an infinite number of equilibrium conditions can be
written down but if a triple (pair for a point) independent equilibrium
conditions are met then all are met, too.
\item The DOFs of parts (bodies and points) add up for structures.
\item Constraints, for instance supports, take the structure's DOFs.
\item When a structure is loaded then in each constraint a
definite force(s) appears depending which DOFs the constraint takes.
Supports take absolute DOFs, internal constraints take relative DOFs.
\item Necessary but not sufficient condition for statical determinacy of    
a structure is that the number of DOFs (= number of independent
equilibrium conditions) exactly equals the number of constraints
(=number of unknown forces).
\item Sufficient condition for statical determinacy is that the forces
in constraints can be determined from the available equilibrium
conditions that is, the number of DOFs must equal the number of
constraints and the system of equilibrium equations must be regular.
\item When the number of constraints is greater than the number of DOFs
then the structure is \emph{statically indeterminate.} 
Then there are more than one
(mostly infinitely many) sets of constraints forces that meet the
equilibrium conditions. This is the root of the term indeterminacy.  
\item If there are less constraints then DOFs then there are less unknown
forces than equations and equilibrium cannot be established for a
general load. The same is true when the number of DOFs equals the
number of constraints but  the system of equilibrium equations
is singular. In these cases the structure is 
\emph{(geometrically) unstable.}
\item In principle, the forces in constraints of a statically
determinate structure can be computed from the
system of coupled linear equilibrium equations of all bodies.
This method is used in computer codes since it is algorithmically simple.
For manual school solutions in this course we better use judicious
selection of the sequence of equilibrium equations. Among infinitely
many equilibrium conditions, almost always such sequence can be found
by which the unknown forces can be determined one by one. The solution
of the system of coupled equations can be avoided. This way has an
important side effect, it promotes the understanding how the forces
act inside the structure and contributes to the 'engineering judgment
 and intuition' of the students.
\end{enumerate}
All the enumerated assertions can be condensed in a simple rule:
\\
\emph{
All external (reactions in supports) and internal forces can be 
determined in
statically determinate structures by equilibrium conditions}.
\\
This rule is valid for all structures, not only for planar frames and
trusses.
It takes some skill to apply the rules in examples and practice.
Students of this course are assumed to have gained the skill in the 
previous courses.
\\
\small{Example 1:
Compare the number of DOFs and constraints in the above structures, decide
on their statical determinacy.
Find the correct answers at the end of the exercise.}

\subsection{Trusses}
Trusses can be perceived as a special type of frames where the beams,
straight, usually slender, play the part of bodies
whereas the internal hinges are perceived as constraints.
Simpler and more convenient, the truss is interpreted
as a set of points (bodies) and the beams as links (constraints).
When applied to the truss in the leftmost sketch below,
the number of DOFs in the first perception is 9x3=27 whereas it is
just 6x2=12 in the second perception. These are also the numbers of the
equilibrium equations to be solved in the two perceptions.
I order to simplify the sketches the hinges in trusses 
will not be indicated by
small circles in this course but \emph{in trusses, each joint is a hinge 
by default}. The second sketch in the figure below shows 
the same structure as the first one if it is declared to be a truss. 
\\
\insFigUp{week1-trusses.pdf}{\textwidth}
\\
Another method of determining statical determinacy is illustrated
in the right half of the above figure. The method can be termed
'build up' method. In the first step the simplest truss is set up,
consisting of three points - joints and three links - members. This
truss would become statically determinate if three external
constraints were supplied. Structure with this property is termed
\emph{internally statically determinate}. In sketch 2 a point is connected
to the simplest truss by two links. The relative DOFs of the point
with respect to the simplest truss are taken (recall that two constraints
are enough to fix a point) and the truss in sketch 2
thus is internally statically determinate, too. In two remaining
sketches two other points are connected in the same manner so that
in the rightmost sketch the truss is internally statically determinate.
When the pin support and roller are added, the truss is 
statically determinate. 

The same method can be used with frames, too. This method checks
the sufficient condition of statical determinacy.
\\
\emph{There is always a single force in a member of a truss regardless
of the member's shape. The ray of the force is the line connecting the 
end hinges. This is true also for any body of a frame which is connected
by two hinges and is not loaded}
\\
\insFigUp{week1-link.pdf}{0.6\textwidth}




\section{Internal forces diagrams}
Internal forces diagrams display the results of structural mechanics for
further steps of design. The conventions used in this course are
demonstrated on an example. A simple beam loaded by a pin force
inclined by $45^o$ is solved for the bending moment diagram.
The steps are given in full detail.
\\
\insFigUp{week1-simplebeam.pdf}{\textwidth}
\\
\\ 1 Reactions\\
Equilbrium condition of the whole beam, moments with respect to point $B$:
\[ whole~beam~ \odot B : -A_y\cdot 10 +F_y\cdot 2=0,~~~
   -A_y \cdot 10 -0.707\cdot 2=0,~~~\Rightarrow~ A_y=-0.141 \]
Reaction $B$ follows from the equilibrium of vertical forces:
\[ ~whole~beam~\uparrow: -0.141 +B -0.707 =0,~~~\Rightarrow B=0.848 \]
2 Bending moment diagram\\
Reaction $A_y$ is sufficient to draw the whole  $M$ diagram.
For instance, $M_{BA}$ at the cross-section infinitely close to
support $B$ from the left side is $M_{BA}=-0.141\cdot10=-1.41$.
See below the third note on the bending moment drawing and sign conventions.
\\
Note: 
\begin{mylist}{-2pt}{10pt}
\item Any equilibrium condition should be labeled. The label should
indicate a) the body, or the part of a body or a set of bodies 
the condition refers to and b)the kind of the condition.
In the example the circle with dot indicates moment equilibrium
of the whole beam with respect to point B, the up arrow the
equilibrium of vertical forces.
\item Moments with respect to a point (usually just moments) are positive 
counterclockwise.
\item Bending moments at a cross-section do not have a general sign 
convention. Their values are thus written without sign in the
bending moment diagrams. The way they act at a cross-section is
indicated by the orientation of their ordinates - in Europe we draw
them on the tension side of the cross-section.
When a bending moment appears in an expression then it must have some
positive sense. For that purpose an ad hoc (for the occasion)
positive sense is established. In horizontal beams the 
	bending moment inducing
tension in lower fibers of the cross-section is usually considered
positive. The $M_{BA}$ is an example.
\item The values in the $N$ and $V$ diagrams should be with correct signs, 
	see the argument below.
\item Arrow with a variable indicates positive sense of the variable,
	reaction $B$ for instance.
\item Arrow with a value indicates the actual action, see the forces
	acting in the right sketch. Just positive values can thus occur.
\end{mylist}
The shear force $V$ and normal force $N$ have a world wide adopted
sign convention, their positive signs are defined in the sketch:
\\
\insFigUp{week1-signsNVM.pdf}{0.6\textwidth}
\\
The contours of the two pieces of the beam are drawn for better
perception.
When the left sketch is viewed from above, the same positive sense
of $N$ and $V$ remains. If the right sketch is viewed from above then
the positive sense of the bending moment is reversed. It justifies
the assertion that no general positive sense can be defined for the
bending moment.

A few defaults will be used throughout the course:
\begin{mylist}{-2pt}{10pt}
\item The beams are straight.
\item The default units are $m$, $kN$. Unless necessary, the units are
not indicated for brevity. In design practice,  units must
be specified for all results.
\item The default coordinate system is right handed cartesian 
with horizontal $x$ axis and upward $y$ axis.
\item All loads and supports and their reactions act directly at
the beam axes. This is an approximation, they actually act at
the beam faces.
\item Uniform continuous load is shown as shaded rectangle, see the
example below. The direction of the shading is the direction
of the load, default action is toward the beam. On inclined beams
the rectangle may change in a parallelogram.
\end{mylist}
Useful relations are recalled among lateral load $f$, shear force $V$
and bending moment $M$, valid in straight beams:
\[ f=-{\dd V \over \dd x},~~~V={\dd M \over \dd x},~~\Rightarrow
 f=-{\dd ^2 M \over \dd x^2}\]
They are the equilibrium conditions of a differential piece $\dd x$ of a
horizontal beam with ad hoc positive sense of load downwards and
positive bending moment pulling at lower face.
They imply such useful rules like \emph{on a stretch without load
the shear force diagram must be constant and bending moment diagram
must be a straight line} and others.



\section{Example frame}
Draw internal forces diagrams in the frame
\\
\insFigUp{week1-frame.pdf}{\textwidth}
\\
\[ whole~frame \odot A: -4\cdot 4/2 + B\cdot 3=0,~~~\Rightarrow~B=2.66 \]
Moment $M_{CB}=M_{CA}=2.66\cdot 3=8$, the ad hoc positive sign is when
the inside fibers are in tension. The shear force diagram can be drawn
right away, the absence of load upon beam CB implies that $V$ must be
constant in the beam, whereas in beam AC it is an inclined straight 
line owing
to the uniform load. In beam AC, the bending moment diagram
is parabolic. The two end values of the bending moment at the end of the
beam are sufficient for drawing the parabola 
since an additional property of the curve is known -
the zero shear force at point $C$ implies that the tangent of the parabola
is prallel to the beam axis at that point. 
The normal force diagram is trivial.
\emph{The diagrams should be drawn at least approximately to scale.
Shear and normal force diagrams to one scale, bending moment to another
scale.} This is an important means of check.


\section{Example truss}
Compute member forces in the truss and display them in a sketch.
\\
\insFigUp{week1-truss.pdf}{0.6\textwidth}
\\
All joints of a truss are hinges by definition including those
which seem to be clamped, joints 1 and 3 in this case.
The method of joints is applied in this simple case. It is based
on a judicious selection of the sequence of the joint's
equilibrium equations. At each step, a joint is to be found where
just two unknown forces act. In this case it is joint 2 with two
unknown forces $N_{1,2}$ and $N_{2,4}$. From two equilibrium
conditions of the joint we have right away that $N_{1,2}=N_{2,4}=0$.
\emph{It is recommended that the known member forces are inserted 
in a truss sketch
immediately since the sketch suggests then the equilibrium codition to
use next.}
The next joint with just two unknown forces is joint 4 with
$.N_{1,4}$ and $N_{3,4}$. The condition in vertical direction is
more suitable because just a single unknown force occurs in it.
\[ 4~\rightarrow: -N_{1,4}\cos(45^o) - 1=0,~~~\Rightarrow N_{1,4}=-1,414 \]
The condition of horizontal forces then yields $N_{3,4}$.
\[ 4~\uparrow: -N_{1,4}\cos(45^o) -N_{1,4}=0,~~\Rightarrow N_{1,4}=-1 \]
\emph{Normal (member) forces in truss member share the sign convention
with normal forces in frame beams - positive when in tension.}
\\ \\
\small{The solution of example 1: a:3DOFs vs 4 constraints, indeterminate,
b:6DOFs vs 5 constraints, instable, c:3DOFs vs 4 constraints, indeterminate,
d:3DOFs vs 3 constraints, instable, the equilibrium of horizontal forces
generally cannot be established.}
\\ \\
Homework 1:
\\
Draw the $N,~V,~M$ diagrams for the two frames:
\\
\insFigUp{week1-hw.pdf}{\textwidth}
\\
\newpage
HW 1, solutions:\\
Frame with three hinges:\\
Reactions are solved for first (this no rule, in some cases internal
forces can be drawn right away):\\
Towards reaction $B_y$:
\[ whole~structure~\odot A: -1\cdot 4\cdot 2 +B_y\cdot 8=0,~\Rightarrow 
  B_y=1 \]
Towards reaction $B_x$:
\[ part~II \odot C: 1\cdot 4 + B_x\cdot 4=0,~\Rightarrow~~B_x=-1 \]
\[  whole~structure~\rightarrow: A_x +B_x=0,~\Rightarrow~~A_x=1 \]
\[  whole~structure~\uparrow: A_y +B_y-4\cdot 1=0,~\Rightarrow~~A_y=3 \]
The forces acting at hinge C upon part I are drawn. \\
\\
\insFigUp{week1-hw-sola.pdf}{0.8\textwidth}
\\
Internal forces diagrams are simple to draw. A simple additional
calculation is necessary for the bending moment diagram on the left
upper beam where the shape is parabola.
The bending moment at the center (ad hoc sign convention: positive when
pulling at lower face):
\[ M_c= 1\cdot2 - 2\cdot 2^2 /2 =0 \]
From the shear force diagram it follows that the extreme of the bending
moment occurs at 3/4 of the beam length (recall that in straight beams
$\dd M / \dd x= V$ so that zero point of $V$ indicates the extreme of $M$).
\\ \\
Frame with the tie:\\
There are three components of reactions. They can be determined from
the equil. conditions of the whole structure:
\[ whole~structure~\odot A: -10\cdot 4 +2\cdot 4\cdot 2 +B\cdot 8=0,~~
 ~\Rightarrow~~B=3\]
\[  whole~structure \uparrow:A_y-10+3=0\]
\[ whole~structure \rightarrow:A_x-2\cdot4=0,~~~A_x=8\]
The positive force in the link $D$ is tension:
\[  part~I~\odot C: 8\cdot4-7\cdot 4+ D\cdot 2=0, \Rightarrow D=-4\]
\\
\insFigUp{week1-hw-solb.pdf}{0.8\textwidth}
\\
The sum of all forces acting upon part II and hinge $C$ acts
at $C$ upon part I, see the force components without labels below and left
from $C$. Most values of internal forces are easy to tell off hand.
For instance $M_{E,A}= -8\cdot 4 +4\cdot 2=24$, ad hoc positive sense
bending moment induces tension at the inside (right) face of the beam $A_E$.
Bending moment at cross-section $c$ is worth of recording (positive when
tension at the inside (left) face):
\[ M_c=-2\cdot 3^2/2 +4\cdot 3=3 \]


  



 

 
 

\end{document}
