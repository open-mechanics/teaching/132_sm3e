# symmetric
# N1 forces
dl1=1;
l1=sqrt(10^2+10^2)
l2=sqrt(5^2+10^2)
c1=10/l1;s1=c1;c2=5/l2;s2=10/l2;
#equil. eq. in  virt. stress state - hor. 1 force
a=[-c1,-c2;-s1,-s2]
f=[1;0]
Ns1=-a\f
D0=Ns1(1)*dl1
d=Ns1(1)^2*l1+Ns1(2)^2*l2
Xs=-D0/d
Ns=Ns1*Xs
#antisymmetric
#equil. eq. in  virt. stress state - vert. 1 force
f=[0;1]
Na1=-a\f
D0=Na1(1)*dl1
d=Na1(1)^2*l1+Na1(2)^2*l2
Xa=-D0/d
Na=Na1*Xa
Nleft=Ns+Na
Nright=Ns-Na
#check hor.
heq=Nleft(1)*c1+Nleft(2)*c2-Nright(1)*c1-Nright(2)*c2
#check vert.
veq=Nleft(1)*s1+Nleft(2)*s2+Nright(1)*s1+Nright(2)*s2
