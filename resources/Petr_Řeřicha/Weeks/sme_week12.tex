\documentclass[12pt]{article}
\usepackage{graphics,my,comb}
\usepackage{makeidx}

\graphicspath{{figures/}}

\oddsidemargin 0mm

\renewcommand{\sketch}{Sketch}
\renewcommand{\arule}{Rule}
\renewcommand\contentsname{Contents}
\renewcommand\refname{References}
\renewcommand\tablename{Table}
\usepackage{times}
\makeindex
%\flushbottom

\begin{document}
\title{ME3E course, slope deflection method, exercise, homework 12}
\author{Petr \v Re\v richa }
%\month{May}
%\year{1997}
%\date{}
\maketitle
%\tableofcontents

\topmargin -15mm
\topmargin -24mm
% the page size required by CVUT Edition center:
\textheight 250mm
\textwidth 160mm
\oddsidemargin 0mm
\footskip 10mm
\pagestyle{plain}
\textfloatsep 0pt

The examples complement section 4.5 of the lecture notes
'force and slope defl. methods' on the course web page, just 'Notes'
forthwith. They
should substitute classes of the present 12th week of the semester.
Questions concerning the stuff, address, please, to\\
'petr.rericha@fsv.cvut.cz'.\\ 
HW 12 should be delivered at the same address
by Tuesday, May, 11th. If possible, process the homework
in a document processor (Word, Latex,..), scanned manuscripts are
acceptable, too. 


The cross-section stiffnesses $EA=1$, $EJ=1$ are assumed in most examples
for brevity unless otherwise stated.

\section{Reference for formulas and variables}
The arrows indicating displacements, rotations, forces and moments define
their positive senses!
\\
The real displacements and rotations are very small, both $\varphi_i$ and $\psi_{i,j}$
are of the order $0.001$ in real structures. The geometric relations valid
for infinitesimal displacements and rotations are therefore used,
for instance $\psi_{i,j}=v/l_{i,j}$. 
\\
\insFigUp{end-forces.pdf}{\textwidth}
\begin{equation}
M_{i,j} = k_{i,j} (2\phi_i + \phi_j -3~\psi_{i,j}) + M_{i,j}^f 
%                                                       \label{eq_sd4}
\end{equation}
$M_{i,j}$ - end moment at joint $i$ of beam $i,j$ owing to the joint
and beam rotations and to the beam load. The beam load is not shown in the left sketch
in order not to obscure the picture, instead its tokens are visible in the right sketch.
\\
$M_{i,j}^f$ - fixed end moment at joint $i$ of beam $i,j$ owing to the beam load alone
 with joint displacements and rotations fixed.
$\varphi_i$ - joint rotation
\\
$\psi_{i,j}$ - beam rotation
\\
A table of the fixed-end moments is provided for easy reference:
\begin{tabl}
\begin{center}
Fixed-end moments
\index{end moments!fixed}%
\begin{tabular}{cccccc}
load & $M_{i,j}^f$ & $M_{j,i}^f$& load & $M_{i,j}^f$ & $M_{j,i}^f$\\
\hline
\insFigUp{sdtab1.pdf}{0.2\textwidth}&${F a b^2 \over l^2}$ & ${-Fa^2b
\over l^2}$
&\insFigUp{sdtab4.pdf}{0.2\textwidth}&$fa^2{6l^2-8al+3a^2\over 12l^2}$ 
& $fa^3{3a-4l\over 12l^2}$\\
\insFigUp{sdtab2.pdf}{0.2\textwidth}&$Mb{2a-b\over l^2}$ & $Ma{2b-a
\over l^2} $
&\insFigUp{sdtab5.pdf}{0.2\textwidth}&$fl^2/20$&$-fl^2/30$ \\
\insFigUp{sdtab3.pdf}{0.2\textwidth}&$fl^2/12$ & $-fl^2/12 $
&\insFigUp{sdtab6.pdf}{0.2\textwidth}&$-{\Delta t~\alpha~EJ\over h}$ 
&${\Delta t~ \alpha~EJ\over h} $
\end{tabular}
\end{center}
\label{tabl_1}
\end{tabl}
A modified formula is valid for the end moment $M_{i,j}$ when end $j$ is attached
to the joint by a hinge:
\\
\insFigUp{end-forces-hinge.pdf}{0.5\textwidth}
\begin{equation}
M_{i,j}= k_{i,j}{3\over 2}(\phi_i-\psi_{i,j}) 
  +M_{i,j}^f-{M_{j,i}^f\over 2}
  ~~~~~~{\rm hinge~at~}j~~~~~~~M_{j,i}=0
%                                                       \label{eq_sd6} 
\end{equation}
If a beam is not loaded then a simple formula follows from its free body equilibrum
condition:
\begin{equation}
V_{i,j}= {M_{i,j}+M_{j,i}\over l_{i,j}}                  
\end{equation}
\\ \\ \\ \\ \\ \\
\section{Use of symmetry, split structure technique}
The next example on the slope deflection method is the structure in
the left sketch in the figure
\\
\insFigUp{exc12-frame4.pdf}{\textwidth}
\\
The frame has 5 independent DOFs in the slope deflection method, three
joint rotations and two beam rotations. It pays to make use of symmetry
in this case. The symmetric and antisymmetric load cases in the central
and right sketches have two DOFs each.
\\ {\bf Symmetric load case} \\
The indepedent DOFs are $\varphi_3$ and $\psi_{1,3}$. When  $\varphi_3$
is imposed, beam $3,5$ rotates with rotation center in point $c$,
consequently $\psi_{2,5}= -4\psi_{1,3}/2= -2\psi_{1,3} $.
The equilibrium equation conjugate to $\varphi_3$ is trivial:
\[ 3 \odot: M_{3,1}+M_{3,5}=0\]
The equation conjugate to $\psi_{1,3}$ is best obtained by the principle
of virtual work. In the central sketch the virtual displacement is shown
by the bold dashed lines. The actual (yet unknown) forces of the actual
stress state do the work
\[ \delta w= (-4\psi_{1,3}/2)\cdot 0.5\cdot 4 +(2\psi_{3,5}/2)\cdot 0.5
\cdot 2  +
 (M_{1,3}+M_{3,1}) \psi_{1,3} +(M_{3,5}+M_{5,3})\psi_{3,5}=0 \]
 upon the virtual displacements. Sustitutions for $\psi_{3,5}$ then
 deliver the equilibrium in terms of the end moments:
 \[ \delta \psi_{1,3}: -6+M_{1,3}+M_{3,1}-2M_{3,5}-2M_{5,3}=0. \]
 Routine applications of formula (1) for the end moments
 supply
 \[ M_{3,1}= 2\varphi -3\psi_{1,3} -0.5\cdot 4^2/12 = 2\varphi -3\psi_{1,3}
 -0.666,~~~M_{1,3}=\varphi -3\psi_{1,3} +0.666 \]
\[ M_{3,5}= 2\cdot1.11\varphi_3-3\cdot 1.11 \psi_{3,5} +
 0.5\cdot(2/\sqrt{13})\cdot 13 /12= 2.22\varphi_3+6.66\psi_{1,3}+0.301 \]
\[ M_{5,3}= 1.11\varphi_3-3\cdot 1.11 \psi_{3,5} -
 -0.5\cdot(2/\sqrt{13})\cdot 13 /12= 1.11\varphi_3+6.66\psi_{1,3}-0.301 \]
 The equilibrium equations in the table form read
 \[ \begin{array}{ccc}\varphi_3&\psi_{1,3}& load term\\
	 4.22&3.66&-0.365\\-3.66&-32.66&-6
  \end{array}
 \]
 with the solution $\varphi_3=0.272$, $\psi_{1,3}=-0.214$. 
 Back substitutions yield
 \[ M_{1,3}=1.58~~~M_{3,1}= 0.525,~~~M_{3,5}=-0.525,~~~M_{5,3}=-1.42\]
{\bf Antisymmetric load case} \\
Independent DOFs are the same as in the symmetric case, $\varphi_3$
and $\psi_{1,3}$. Vertical translations of joints $3$ and $5$ are 
restrained therefore beam $3,5$ cannot rotate, $\psi_{3,5}=0$
(rotation center of beam $3,5$ lies in infinity in vertical direction
as indicated in the right sketch in the figure). The equil. condition
conjugate to $\varphi_3$ is the same as in the symmetric load case
in terms of the end moments
\[3\odot: M_{3,1}+M_{3,5}=0\]
The equil. equation conjugate to $\psi_{1,3}$ is derived using the equil.
condition of beam $3,5$ in horizontal direction, the separating cut is in
beam $1,3$ adjacent to joint $3$:
\[ beam 3,5 \rightarrow: -V_{3,1}+0.5\cdot 2 =0,~~~\Rightarrow
  -\left({M_{1,3}+M_{3,1}\over 4} - {0.5\cdot 4\over 2}\right)+1~~~
  \Rightarrow M_{1,3}+M_{3,1}-8=0 \]
The expressions for $M_{1,3}$ and $M_{3,1}$ remain the same as in the
symmetric load case and
\[ M_{3,5}=1.5\cdot 1.11\varphi_3 +\sqrt{13}/12-0.5(-\sqrt{13}/12)=
 1.661\varphi_3 + 0.45 \]
 The equil. conditions in table form
 \[ \begin{array}{ccc}\varphi_3&\psi_{1,3}& load term\\
	 3.66&-3&-0.215\\3&-6&-8
  \end{array}
 \]
 yield the solution $\varphi_3=-1.75$, $\psi_{1,3}=-2.21$. 
 Back substitutions give the end moments
 \[ M_{1,3}=5.54.~~~M_{3,1}=2.46,~~~M_{3,5}=-2.46\]
 The end moments are shown for the symmetric and antisymmetric load cases
 in the center and right sketches of the bottom row in the above figure.
 The tension side oriented ordinates and the absolute values of the
 end moments are used. The lines of the bending moment diagrams are not
 given in those two intermediate results. In the left sketch the final
 bending moment diagram is drawn. The values and ordinates of the end
 moments are obtained by superposition of the symmetric and antisymmetric
 load cases, note that on the right part of the frame the end moments
 ordinates of the symmetric load case are the mirror images of those
 on the left part.
 The bending moments at beam's $1,3$ and $3,5$ centers are obtained
 by superposition of the end moments  effects, the value at beam center
 is the average value of the end moments, with the simple beam
 bending moment diagrams.
\section{Use of symmetry, whole structure technique}
The frame in the next figure has a minimum of 4 DOFs, $\varphi_3$,
$\varphi_4$, $\psi_{1,3}$ and $\psi_{3,5}$. The structure and load
are symmetric, no load decomposition is necessary. When the split 
structure technique is used, the symmetric half has 3 DOFs as a
minimum, $\varphi_3$, $\psi_{3,7}$ and $\psi_{5,8}$, see the
cente sketch in the figure.
\\
\insFigUp{exc12-frame5.pdf}{\textwidth}
\\
The beam stiffnesses are indicated in the sketch. We put $k=1$
for brevity, the result forces and moments have to be multiplied
by the actual value of $k$.
Application of symmetry in the whole structure implies
\[ \varphi_3=-\varphi_4,~~~\psi_{1,3}=0, \psi_{3,5}=\alpha t\cdot 4/4=
 =\alpha t, \]
 so that just a single DOF remains, $\varphi_3$.
 The conjugate equil. condition is obvious
 \[ 3 \odot:  M_{3,1}+M_{3,4}+M_{3,5}= 0 \]
 Expressions for the end moments
 \[M_{3,1}= 2\varphi_3,~~~M_{3,4}= 4(2\varphi_3- \varphi_3)=4\varphi_3,~~~
  M_{3,5}=1.5\varphi_3  -1.5\alpha t\]
  The equil. equation reads
  \[ 7.5\varphi_3 -1.5\alpha t=0,~~~\Rightarrow~ \varphi_3=0.2\alpha t\]
  Note that joint rotation $\varphi_3$ is the actual one. Standard
  back substitutions provide
  \[ M_{3,1}=0.4\alpha t,~~~M_{3,4}=0.8\alpha t,~~~M_{3,5}=-1.2\alpha t,~~~
  M_{1,3}=0.2\alpha t \]
  The bending moment diagram is in the right sketch in the above figure
  without the factor $\alpha t$. In order to obtain true numbers,
  the values in the diagram  are to be multiplied by $k\alpha t$.
  \\
  {\bf HW 12:}
  \\
Solve by the slope deflection method, draw all internal forces diagrams.
\\
\insFigUp{exc12-hw.pdf}{0.5\textwidth}
\\
\newpage
{\bf HW 12, Solution}\\
The frame has at least 3 DOFs in the slope deflection method,
$\varphi_3$, $\varphi_4$ and $\psi_{1,3}$. Rotations of beams
$3,4$ and $2,4$ depend on $\psi_{1,3}$. The task can be substantially
simplified when symmetry is utilized.
\\ \insFigUp{exc12-hw-sol.pdf}{\textwidth} \\
\\
{\bf Symmetric load case}
\\
Translation of joint 3 is restrained, two DOF in the slope deflection
method exist, $\varphi_3$ and $\psi_{3,5}$. The two conjugate equil.
equations are
\[ 3 \odot: M_{3,1}+M_{3,5}=0 \]
\[ 5 \uparrow: V_{5,3}=0,~~~\Rightarrow~M_{3,5}+M_{5,3}=0 \]
The end moments:
\[ M_{3,1}=1.5\varphi,~~~M_{3,5}=2\cdot2.5\varphi_3-3\cdot2.5\psi_{3,5},~~~
  M_{5,3}=2.5\varphi_3-3\cdot2.5\psi_{3,5} \]
It turns out that the equilibrium equations are homogeneous in the DOFs
that is, the load terms are zeros.
\[ \begin{array}{ccc} \varphi_3&\psi_{3,5}& load term\\ 6.5&-7.5&0\\
7.5&-15&0 \end{array} \]
Then both DOFs are zeros, too and so are the bending moments and shear
forces in the frame. Normal forces are computed from the equil. condition
of joint $3$:
\[ 3\uparrow: 0.5+N_{3.1}\cdot4/5=0,~~~\Rightarrow~~N_{3.1}=-0.625 \]
\[ 3\rightarrow N_{3,1}\cdot 3/5-N_{3,5}=0,~~~\Rightarrow~~-N_{3,5}=-0.375\]
\\
{\bf Antisymmetric load case} 
\\
The frame is statically determinate, reactions are shown in the left
sketch in the figure below. 
The bending moment and shear force diagrams of the antisymmetric
load case are at the same time the final diagrams:
\\
\insFigUp{exc12-hw-sol2.pdf}{\textwidth}
\\
The antisymmetric bending moment diagram on the whole frame is
drawn in the bottom sketch. The final normal force is the superposition
of the symmetric and antisymmetric parts shown in the right bottom sketch.



  

 
\end{document}
